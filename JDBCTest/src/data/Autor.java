package data;

import java.time.LocalDate;

public class Autor
{
    private long id;
    private String vorname;
    private String nachname;
    private LocalDate geburtsJahr;

    public Autor(long id, String vorname, String nachname, LocalDate geburtsJahr) throws Exception
    {
        setId(id);
        setVorname(vorname);
        setNachname(nachname);
        setGeburtsJahr(geburtsJahr);
    }

    public Autor()
    {

    }

    public long getId()
    {
        return id;
    }

    public void setId(long id) throws IllegalArgumentException
    {
        if(id >= 1)
        {
            this.id = id;
        }
        else throw new IllegalArgumentException("illegal id");
    }

    public String getVorname()
    {
        return vorname;
    }

    public void setVorname(String vorname) throws IllegalArgumentException
    {
        if(vorname.length() > 0)
        {
            this.vorname = vorname;
        }
        else throw new IllegalArgumentException("Vorname nicht eingegeben");
    }

    public String getNachname()
    {
        return nachname;
    }

    public void setNachname(String nachname) throws IllegalArgumentException
    {
        if(nachname.length() > 0)
        {
            this.nachname = nachname;
        }
        else throw new IllegalArgumentException("Nachname nicht eingegeben");
    }

    public LocalDate getGeburtsJahr()
    {
        return geburtsJahr;
    }

    public void setGeburtsJahr(LocalDate geburtsJahr) throws NullPointerException
    {
        if(geburtsJahr != null)
        {
            this.geburtsJahr = geburtsJahr;
        }
        else throw new NullPointerException("Null ubergegeben");
    }
}
