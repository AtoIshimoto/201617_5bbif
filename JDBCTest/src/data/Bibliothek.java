package data;

import java.util.ArrayList;
import java.util.List;

public class Bibliothek
{
    private long id;
    private String addresse;
    private String plz;
    private String ort;
    private List<Buch> buecher;

    public Bibliothek(long id, String addresse, String plz, String ort, List<Buch> buecher) throws Exception
    {
        setId(id);
        setAddresse(addresse);
        setPlz(plz);
        setOrt(ort);
        setBuecher(buecher);
    }

    public Bibliothek()
    {
        buecher = new ArrayList<Buch>();
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id) throws IllegalArgumentException
    {
        if(id >= 1)
        {
            this.id = id;
        }
        else throw new IllegalArgumentException("illegal id");
    }

    public String getAddresse()
    {
        return addresse;
    }

    public void setAddresse(String addresse) throws IllegalArgumentException
    {
        if(addresse.length() > 0)
        {
            this.addresse = addresse;
        }
        else throw new IllegalArgumentException("addresse nicht eingegeben");
    }

    public String getPlz()
    {
        return plz;
    }

    public void setPlz(String plz) throws IllegalArgumentException
    {
        if(plz.length() == 4)
        {
            this.plz = plz;
        }
        else throw new IllegalArgumentException("plz falsch eingegeben");
    }

    public String getOrt()
    {
        return ort;
    }

    public void setOrt(String ort) throws IllegalArgumentException
    {
        if(ort.length() > 0)
        {
            this.ort = ort;
        }
        else throw new IllegalArgumentException("Ort nicht eingegeben");
    }

    public List<Buch> getBuecher()
    {
        return buecher;
    }

    public void setBuecher(List<Buch> buecher) throws Exception
    {
        if(buecher != null)
        {
            if (buecher.size() > 0)
            {
                this.buecher = buecher;
            }
            else throw new Exception("kein Buch ubergegeben");
        }
        else throw new NullPointerException("Null ubergegeben");
    }

    public void addBuch(Buch buch) throws NullPointerException
    {
        if(buch != null)
        {
            buecher.add(buch);
        }
        else throw new NullPointerException("Null ubergegeben");
    }
}
