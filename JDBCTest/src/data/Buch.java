package data;

import java.time.LocalDate;

public class Buch
{
    private long id;
    private Bibliothek bibliothek;
    private Autor autor;
    private String titel;
    private LocalDate erscheinungsDatum;

    public Buch(long id, Bibliothek bibliothek, Autor autor, String titel, LocalDate erscheinungsDatum) throws Exception
    {
        setId(id);
        setBibliothek(bibliothek);
        setAutor(autor);
        setTitel(titel);
        setErscheinungsDatum(erscheinungsDatum);
    }

    public Buch()
    {

    }

    public long getId()
    {
        return id;
    }

    public void setId(long id) throws IllegalArgumentException
    {
        if(id >= 1)
        {
            this.id = id;
        }
        else throw new IllegalArgumentException("illegal id");
    }

    public Bibliothek getBibliothek()
    {
        return bibliothek;
    }

    public void setBibliothek(Bibliothek bibliothek) throws NullPointerException
    {
        if(bibliothek != null)
        {
            this.bibliothek = bibliothek;
        }
        else throw new NullPointerException("uebergegebener Wert ist null");
    }

    public Autor getAutor()
    {
        return autor;
    }

    public void setAutor(Autor autor) throws NullPointerException
    {
        if(autor != null)
        {
            this.autor = autor;
        }
        else throw new NullPointerException("uebergegebener Wert ist null");
    }

    public String getTitel()
    {
        return titel;
    }

    public void setTitel(String titel) throws IllegalArgumentException
    {
        if(titel.length() > 0)
        {
            this.titel = titel;
        }
        else throw new IllegalArgumentException("Titel nicht eingegeben");
    }

    public LocalDate getErscheinungsDatum()
    {
        return erscheinungsDatum;
    }

    public void setErscheinungsDatum(LocalDate erscheinungsDatum) throws NullPointerException
    {
        if(erscheinungsDatum != null)
        {
            this.erscheinungsDatum = erscheinungsDatum;
        }
        else throw new NullPointerException("erscheinungsDatum ist null");
    }
}
