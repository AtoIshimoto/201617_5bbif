package data;

public class Kunde
{
    private long id;
    private String vorname;
    private String nachname;
    private String addresse;
    private String plz;
    private String ort;

    public Kunde(long id, String vorname, String nachname, String addresse, String plz, String ort) throws IllegalArgumentException
    {
        setId(id);
        setVorname(vorname);
        setNachname(nachname);
        setAddresse(addresse);
        setPlz(plz);
        setOrt(ort);
    }

    public Kunde()
    {

    }

    public long getId()
    {
        return id;
    }

    public void setId(long id) throws IllegalArgumentException
    {
        if(id >= 1)
        {
            this.id = id;
        }
        else throw new IllegalArgumentException("illegal id");
    }

    public String getVorname()
    {
        return vorname;
    }

    public void setVorname(String vorname) throws IllegalArgumentException
    {
        if(vorname.length() > 0)
        {
            this.vorname = vorname;
        }
        else throw new IllegalArgumentException("Vorname nicht eingegeben");
    }

    public String getNachname()
    {
        return nachname;
    }

    public void setNachname(String nachname) throws IllegalArgumentException
    {
        if(nachname.length() > 0)
        {
            this.nachname = nachname;
        }
        else throw new IllegalArgumentException("Nachname nicht eingegeben");
    }

    public String getAddresse()
    {
        return addresse;
    }

    public void setAddresse(String addresse) throws IllegalArgumentException
    {
        if(addresse.length() > 0)
        {
            this.addresse = addresse;
        }
        else throw new IllegalArgumentException("Addresse nicht eingegeben");
    }

    public String getPlz()
    {
        return plz;
    }

    public void setPlz(String plz) throws IllegalArgumentException
    {
        if(plz.length() == 4)
        {
            this.plz = plz;
        }
        else throw new IllegalArgumentException("PLZ falsch eingegeben");
    }

    public String getOrt()
    {
        return ort;
    }

    public void setOrt(String ort) throws IllegalArgumentException
    {
        if(ort.length() > 0)
        {
            this.ort = ort;
        }
        else throw new IllegalArgumentException("Ort nicht eingegeben");
    }
}
