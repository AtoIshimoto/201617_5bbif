package test;

import data.Autor;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class AutorTest
{
    @Test
    public void testId() throws Exception
    {
        Autor autor = new Autor();
        autor.setId(500);

        Assert.assertEquals(500, autor.getId());
    }

    @Test
    public void testVorname() throws Exception
    {
        Autor autor = new Autor();
        autor.setVorname("Billy");

        Assert.assertEquals("Billy", autor.getVorname());
    }

    @Test
    public void testNachname() throws Exception
    {
        Autor autor = new Autor();
        autor.setNachname("Cadacio");

        Assert.assertEquals("Cadacio", autor.getNachname());
    }

    @Test
    public void testGeburtsJahr() throws Exception
    {
        Autor autor = new Autor();
        autor.setGeburtsJahr(LocalDate.now());

        Assert.assertEquals(LocalDate.now(), autor.getGeburtsJahr());
    }
}