package test;

import data.Bibliothek;
import data.Buch;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BibliothekTest
{
    @Test
    public void testId() throws Exception
    {
        Bibliothek bibliothek = new Bibliothek();
        bibliothek.setId(20);

        Assert.assertEquals(20, bibliothek.getId());
    }

    @Test
    public void testAddresse() throws Exception
    {
        Bibliothek bibliothek = new Bibliothek();
        bibliothek.setAddresse("Wopenkastrasse");

        Assert.assertEquals("Wopenkastrasse", bibliothek.getAddresse());
    }

    @Test
    public void testPlz() throws Exception
    {
        Bibliothek bibliothek = new Bibliothek();
        bibliothek.setPlz("1100");

        Assert.assertEquals("1100", bibliothek.getPlz());
    }

    @Test
    public void testOrt() throws Exception
    {
        Bibliothek bibliothek = new Bibliothek();
        bibliothek.setOrt("Burgenland");

        Assert.assertEquals("Burgenland", bibliothek.getOrt());
    }

    @Test
    public void testBuecher() throws Exception
    {
        Bibliothek bibliothek = new Bibliothek();
        Buch buch1 = new Buch();
        Buch buch2 = new Buch();
        List<Buch> buecher = new ArrayList<Buch>();
        buecher.add(buch1);
        buecher.add(buch2);
        bibliothek.setBuecher(buecher);

        Assert.assertEquals(buecher, bibliothek.getBuecher());
    }

    @Test
    public void testAddBuch() throws Exception
    {
        Bibliothek bibliothek = new Bibliothek();
        Buch buch1 = new Buch();
        Buch buch2 = new Buch();
        bibliothek.addBuch(buch2);

        Assert.assertEquals(buch2, bibliothek.getBuecher().get(0));
    }
}