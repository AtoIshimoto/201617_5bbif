package test;

import data.Autor;
import data.Bibliothek;
import data.Buch;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class BuchTest
{
    @Test
    public void testId() throws Exception
    {
        Buch buch = new Buch();
        buch.setId(1);

        Assert.assertEquals(1, buch.getId());
    }

    @Test
    public void testBibliothek() throws Exception
    {
        Buch buch = new Buch();
        Bibliothek bibliothek1 = new Bibliothek();
        Bibliothek bibliothek2 = new Bibliothek();
        buch.setBibliothek(bibliothek1);

        Assert.assertEquals(bibliothek1, buch.getBibliothek());
    }

    @Test
    public void testAutor() throws Exception
    {
        Buch buch = new Buch();
        Autor autor1 = new Autor();
        Autor autor2 = new Autor();
        buch.setAutor(autor2);

        Assert.assertEquals(autor2, buch.getAutor());
    }

    @Test
    public void setTitel() throws Exception
    {
        Buch buch = new Buch();
        buch.setTitel("How to Code");

        Assert.assertEquals("How to Code", buch.getTitel());
    }

    @Test
    public void setErscheinungsDatum() throws Exception
    {
        Buch buch = new Buch();
        buch.setErscheinungsDatum(LocalDate.now());

        Assert.assertEquals(LocalDate.now(), buch.getErscheinungsDatum());
    }
}