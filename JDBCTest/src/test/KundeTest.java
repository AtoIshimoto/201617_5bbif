package test;

import data.Kunde;
import org.junit.Assert;

public class KundeTest
{
    @org.junit.Test
    public void testId() throws Exception
    {
        Kunde kunde = new Kunde();
        kunde.setId(3);

        Assert.assertEquals(3, kunde.getId());
    }

    @org.junit.Test
    public void testVorname() throws Exception
    {
        Kunde kunde = new Kunde();
        kunde.setVorname("Alex");

        Assert.assertEquals("Alex", kunde.getVorname());
    }

    @org.junit.Test
    public void testNachname() throws Exception
    {
        Kunde kunde = new Kunde();
        kunde.setNachname("Ishimoto");

        Assert.assertEquals("Ishimoto", kunde.getNachname());
    }

    @org.junit.Test
    public void testAddresse() throws Exception
    {
        Kunde kunde = new Kunde();
        kunde.setAddresse("Spengergasse");

        Assert.assertEquals("Spengergasse", kunde.getAddresse());
    }

    @org.junit.Test
    public void testPlz() throws Exception
    {
        Kunde kunde = new Kunde();
        kunde.setPlz("1110");

        Assert.assertEquals("1110", kunde.getPlz());
    }

    @org.junit.Test
    public void testOrt() throws Exception
    {
        Kunde kunde = new Kunde();
        kunde.setOrt("Wien");

        Assert.assertEquals("Wien", kunde.getOrt());
    }
}